# stage-firmware-updates

This playbook does the following:
- Check and correct BIOS boot order via DRAC SSH interface
- Stage firmware updates

## Invoking

"f5" is optional, it dispatches 5 forks at a time, reducing overall strain
on the infrastructure.

"--ask-pass" prompts for the DRAC password.

```
ansible-playbook --ask-pass -f5 -i elastic.hosts stage-firmware-updates
```

## Misc notes

- You need SRE-level access to run the playbook.
- You will also need the Dell-provided BIN file to perform a firmware update.
